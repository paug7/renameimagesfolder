<?php
/**
 * Created by Pau G.
 * 10.10.2017
 * Thatzad
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function scanParentDir(){
    $productsReferences = scandir('products');
    foreach($productsReferences as $productReference){
        if(file_exists('products/'.$productReference) && $productReference != '.' && $productReference != '..'){
            changesNamesToParentDir($productReference);
        }
    }
    echo "All files Okey";
    die();
}


function changesNamesToParentDir($reference){
    $files = scandir('products/'.$reference);
    foreach($files as $file){
        if(file_exists('products/'.$reference.'/'.$file) && $file != '.' && $file != '..'){
            $filename = pathinfo('products/'.$reference.'/'.$file);
            rename('products/'.$reference.'/'.$file,     
                   'products/'.$reference.'/'. changesNamesToImages($filename['basename']));
        }
    }
}

function changesNamesToImages($name){

    $newName = "";
    $search  = array(' ', '(', ')');
    $replace = array('', '-', '');
    $newName = str_replace($search, $replace, $name);

    return $newName;
}

scanParentDir();
?>